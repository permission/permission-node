#!/usr/bin/env bash
set -ex

echo "Running run-geth.sh"

CONFIG_FILE="/opt/blockchain/config/config.json"
function getConfig() {
    cat $CONFIG_FILE | jq -r $1
}

NODE_TYPE=$(getConfig '.nodeType')

BOOTNODES_JSON_URL=$(getConfig '.bootNodesUrl')
BOOTNODES_ARG_VAL=$(curl -s ${BOOTNODES_JSON_URL} | jq -r '. | join(",")')

NETWORK_ID=$(getConfig '.networkId')

NODE_NAME=$(getConfig '.nodeName')

BLOCKCHAIN_PORT=$(getConfig '.blockchainPort')

ADDITIONAL_ARGS=$(getConfig '.additionalArgs')

NAT_IP=$(getConfig '.natIp')

COMMON_ARGS="\
    --identity ${NODE_NAME} \
    --datadir /opt/blockchain/data/ \
    --keystore /opt/blockchain/node/keystore \
    --bootnodes ${BOOTNODES_ARG_VAL} \
    --networkid ${NETWORK_ID} \
    --syncmode full \
    --port ${BLOCKCHAIN_PORT} \
    --rpc \
    --rpcaddr localhost \
    --rpcport 8545 \
    --maxpeers 100 \

    ${ADDITIONAL_ARGS}"

if [[ "${NAT_IP}" != "" ]];
then
    COMMON_ARGS="\
        ${COMMON_ARGS} \
        --nat extip:${NAT_IP}"
fi

ALL_ARGS=$COMMON_ARGS

if [[ "${NODE_TYPE}" == "auth" ]];
then
    TARGET_GAS_LIMIT=$(getConfig '.targetGasLimit')
    GAS_PRICE=$(getConfig '.gasPrice')

    exec geth $ALL_ARGS \
        --targetgaslimit ${TARGET_GAS_LIMIT} \
        --gasprice ${GAS_PRICE} \
        --rpcapi 'personal,db,eth,net,web3,txpool,admin,debug,clique,miner'

elif [[ "${NODE_TYPE}" == "boot" ]];
then
    exec geth ${ALL_ARGS} \
        --nodekey /opt/blockchain/node/boot.key \
        --rpcapi 'personal,db,eth,net,web3,txpool,admin,debug,clique'
else
    echo "Node type ${NODE_TYPE} not recognized (auth|boot)"
    exit 1
fi
